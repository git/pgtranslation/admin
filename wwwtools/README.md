Branching
---------

The `postgresql` and `messages` repositories use the same branch
names, but the branching is not necessarily done at the same time.
For instance, the `master` branch usually refers to the most recent
major release for a few month after release.  Only sometime before the
next major release, we create a back-branch (`RELxxx`) in the
`messages` repository and use the `master` branch for the next major
PostgreSQL release.

This can be confusing, but as long as the web interface shows the
right things, it's manageable.

When anything about the branching is to change, the following places
might need to be updated:

- Actually create a new branch and publish it, e.g.,

		git checkout master
		git pull
		git checkout -b REL9_5_STABLE
		git push -u origin REL9_5_STABLE

- Update `update-nls-www`: This controls which branches are processed.

- Update `messages-branches.txt` and `postgresql-branches.txt`: This maps to the actual branch names.

- Update `nls-status-page`: This determines how the branches show up on the web page.
